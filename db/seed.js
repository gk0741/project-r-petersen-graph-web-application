const {Pool} = require('pg');

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'ProjektRPetersen',
    password: 'bazepodataka',
    port: 5432,
});

const sql_create_users = `CREATE TABLE users (
    user_id SERIAL PRIMARY KEY,
    user_name text NOT NULL UNIQUE,
    first_name text NOT NULL,
    last_name text NOT NULL,
    password text NOT NULL
)`;

const sql_create_questions = `CREATE TABLE questions (
    question_id int PRIMARY KEY,
    question_text text NOT NULL,
    question_hint text NOT NULL,
    question_fact text,
    question_image_url text
)`;
const sql_create_answers = `CREATE TABLE answers (
    answer_id int NOT NULL,
    answer_text text NOT NULL,
    is_correct boolean NOT NULL,
    PRIMARY KEY(answer_id, answer_text)
)`;

const sql_create_quizes = `CREATE TABLE quizes (
    quiz_id int PRIMARY KEY,
    user_id int NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    bodovi int NOT NULL,
    datumIgre date NOT NULL
)`;

const sql_create_sessions = `CREATE TABLE session (
    sid varchar NOT NULL COLLATE "default",
    sess json NOT NULL,
    expire timestamp(6) NOT NULL
  )
  WITH (OIDS=FALSE);`

const sql_create_session_index1 = `ALTER TABLE session ADD CONSTRAINT session_pkey PRIMARY KEY (sid) NOT DEFERRABLE INITIALLY IMMEDIATE`
const sql_create_session_index2 = `CREATE INDEX IDX_session_expire ON session(expire)`

const sql_insert_users = `INSERT INTO users (user_name, first_name, last_name, password) VALUES ('aadmin', 'admin', 'adminko', 'aadmin'),
                          ('testiranje', 'test', 'testić', 'testiranje')`

const sql_insert_questions = `INSERT INTO questions (question_id, question_text, question_hint, question_fact, question_image_url)
    VALUES 
    (1, 'Koji je po slici ciklus duljine 8?' , 'Ciklus počinje i završava u istoj točki', 'Petersenov graf sadrži cikluse duljine 5, 6, 8 i 9', './images/question1.png'),
    (2, 'Koliko grafova na slici su Petersenovi?' , 'nema', 'Svi prikazani grafovi su Petersenovi', './images/question2.png'),
    (3, 'Je li Petersenov graf planaran?' , 'nema', 'Kuratowskijev teorem, 1930. godine: "Graf je planaran onda i samo onda ako ne sadrži podgraf homeomorfan s K5 ili K3,3"', './images/pGraf.svg'),
    (4, 'Prvotno je pretpostavljeno da su svi 3-regularni grafovi bez mostova bridno 3-obojivi. Koji graf je poslužio kao protuprimjer toj propoziciji?', 'nema', 'Peter Guthrie Tait je izveo ovu propoziciju iz teorema četiriju boja', 'nema'),
    (5, 'Je li Petersenov graf hamiltonov?' , 'Hamiltonov graf je graf koji sadrži ciklus koji prolazi svim vrhovima zadanog grafa', 'Za graf G kažemo da je hipohamiltonov ako ne sadrži Hamiltonov ciklus, ali uklanjanjem proizvoljnog vrha iz G dobivamo Hamiltonov graf', './images/pGraf.svg'),
    (6, 'Je li izjava: "Kromatski broj ∆-regularnog grafa je uvijek ∆." istinita?' , 'nema', 'Protuprimjeri su grafovi Q3, K4, C3', 'nema'),
    (7, 'Kakav tip (Δ,g)-rešetke je Petersenov graf?' , 'Drugi broj je broj struka grafa', 'Do sada je pronađeno samo 38 regularnih grafova rešetki','./images/pGraf.svg'),
    (8, 'Koji od navedenih skupova bridova grafa su rezni skupovi duljine 5?' , 'Nijedan pravi podskup reznog skupa nije rastavljajući', 'Najmanja duljina reznog skupa Petersenovog grafa je 3', './images/question1.png'),
    (9, 'Koliki je zbroj stupnjeva svih vrhova Petersenovog grafa?' , 'nema', 'Broj vrhova neparnog stupnja u svakom grafu je paran', 'nema'),
    (10, 'Koliki je struk Petersenovog grafa?' , 'Najkraći zatvoreni put', 'nema', './images/pGraf.svg'),
    (11, 'Jesu li grafovi na slici izomorfni?' , 'nema', 'Ovo su izomorfni Petersenovi grafovi', './images/question3.png'),
    (12, 'Koliki je dijametar Petersenovog grafa?' , 'nema', 'Petersenov graf je najveći 3-regularan graf dijametra 2', './images/pGraf.svg'),
    (13, 'Koliki je kromatski broj Petersenovog grafa?' , 'nema', 'Kromatski broj Petersenovog grafa je 3, a njegov bridno kromatski broj je 4', './images/pGraf.svg'),
    (14, 'Što su snarkovi?' , 'nema', 'Sa lijeve strane nalazi se cvjetni snark s 20 vrhova, a s desne strane nalazi se dvostruka zvijezda snark', './images/question4.png'),
    (15, 'Kako se zove šetnja u grafu izražena koracima 3 → 7 → 8 → 9 → 5 → 6 → 1 → 2 → 3?' , 'Počinje i završava u istoj točki', 'nema', './images/question1.png'),
    (16, 'Koje od navedenih svojstava nisu svojstva snarkova?' , 'Prouči slike', 'Petersenov graf je najmanji snark i jedinstven je po tome što ima 10 vrhova', './images/question4.png'),
    (17, 'Koji je poznati hrvatski matematičar otkrio ova dva snarka?' , 'nema', 'Hrvatska pošta je 2000. godine izdala i promovirala marku koja je na sebi sadržavala Blanušin snark', './images/question5.png'),
    (18, 'Julius Petersen je osim otkrića Petersenovog grafa dokazao teorem koji glasi: "Povezan 3-regularan graf sadrži 1-faktor"' , 'nema', '1-faktor je savršeno sparivanje u grafu', './images/pGraf.svg'),
    (19, 'Je li Petersenov graf tranzitivan?' , 'nema', 'Postoje samo 12 konačnih, povezanih, 3-regularnih, tranzitivnih grafova', './images/pGraf.svg'),
    (20, 'Na slici je istaknuto svojstvo Petersenovog grafa, koja je to vrsta grafa?' , 'Obrati pažnju na bridove', 'nema', './images/question6.png'),
    (21, 'Kojem je od navedenih grafova Petersenov graf komplement?' , 'nema', 'nema', './images/pGraf.svg'),   
    (22, 'Koja od ovih osobnosti nije osobnost Kneserovog grafa?' , 'nema', 'Petersenov graf je Kneserov graf K(5,2)', './images/question7.png'),
    (23, 'Je li Petersenov graf bipartitan?' , 'nema', 'Vrhove Petersenovog grafa ne možemo podijeliti u dva skupa tako da sve veze idu iz jednog skupa u drugi', './images/question8.png'), 
    (24, 'Kako nazivamo vrh stupnja 0, a kako nazivamo vrh stupnja 1?', 'nema', 'nema', './images/question9.png'),
    (25, 'Koji je naziv za lemu: "U svakom grafu G je zbroj stupnjeva svih vrhova paran"' , 'nema', 'Prilikom rukovanja bilo kojeg broja ljudi, broj ruku je nužno paran', 'nema'),
    (26, 'Koliki je križni broj Petersenovog grafa?' , 'Prouči slike', 'nema', './images/question10.png'),
    (27, 'Jesu li kocke Q1, Q2, Q3 planarne?' , 'nema', 'Graf Q4 ima n=16 vrhova i m=32 brida te zbog formule m <= 2n - 4 nije planaran', './images/question11.png'),
    (28, 'Koliki je kromatski broj grafa na slici?' , 'nema', 'nema', './images/question12.png'),
    (29, 'Petersenov graf je jako regularan s parametrima (10,3,x,y), za x vrijedi svaka dva susjedna vrha imaju točno x zajedničkih susjeda, a za y vrijedi svaka dva nesusjedna vrha imaju točno y zajedničkih susjeda, odredi x i y Petersenovog grafa' , 'nema', 'nema', './images/pGraf.svg'),
    (30, 'Je li graf na slici bipartitan?' , 'nema', 'Vrhove ovog grafa možemo podijeliti u dva skupa tako da sve veze idu iz jednog skupa u drugi', './images/question13.png'),
    (31, 'Koji od sljedećih svojstava vrijedi za karte?' , 'nema', 'Problem četiriju boja za karte je ekvivalentan problemu četiriju boja za planarne grafove', './images/question14.png'),
    (32, 'Kako označavamo kromatski broj grafa G?' , 'nema', 'Brooks, 1941. godine: "Ako je G jednostavni povezani graf koji nije potpun, te ako je najveci stupanj nekog vrha jednak ∆ (∆ ≥ 3), onda je G ∆-obojiv"', './images/question14.png'),
    (33, 'Tko je i kada otkrio problem četiriju boja?' , 'Otkriven je 1852. godine', 'Francis Guthrie ga je otkrio 1852. godine kada je bojio karte engleske grofovije', './images/question15.png'),
    (34, 'Koji graf služi kao protuprimjer teoremu koji glasi: "Svaki povezan graf koji je tranzitivan po vrhovima sadrži Hamiltonov put"?' , 'nema', 'nema', './images/question2.png'),
    (35, 'Je li potpuni graf K5 Hamiltonov?' , 'nema', 'Potpuni graf s n vrhova uvijek hamiltonovski', './images/question16.png'),
    (36, 'Što vrijedi za kromatski indeks grafa G ako je G jednostavan graf s najvećim stupnjem nekog vrha jednakim ∆?' , 'nema', 'Kromatski indeks Petersenovog grafa je 4', './images/pGraf.svg');
`;

const sql_insert_answers = `INSERT INTO answers (answer_id, answer_text, is_correct) VALUES 
    (1, '3 → 7 → 8 → 0 → 1 → 6 → 5 → 9 → 2', FALSE),
    (1, '0 → 4 → 5 → 9 → 8 → 7 → 6 → 1 → 0', TRUE),
    (1, '4 → 5 → 9 → 8 → 7 → 6 → 1 → 2 → 3 → 4', FALSE),
    (1, '4 → 5 → 6 → 7 → 3 → 2 → 9 → 5 → 4', FALSE),

  (2, '1, 2, 3, 5, 6, 8', FALSE),
    (2, '1, 2, 3, 4, 6, 8', FALSE),
    (2, '1, 2, 3, 5, 7, 8', FALSE),
    (2, 'Svih 8', TRUE),

  (3, 'Da, zato što je stezljiv do K5', FALSE),
    (3, 'Ne, zato što sadrži podgraf K3,3', TRUE),
    (3, 'Ne, zato što sadrži podgraf stezljiv do K5', TRUE),
    (3, 'Da, zato što je stezljiv do K3,3', FALSE),

  (4, 'Kocka', FALSE),
    (4, 'Petersenov graf', TRUE),
    (4, 'Tetraedar', FALSE),
    (4, 'Haewoodov graf', FALSE),

  (5, 'Da, graf je hamiltonov', FALSE),
    (5, 'Graf je hipohamiltonov', TRUE),
    (5, 'Ne, graf nije hamiltonov', FALSE),

  (6, 'Da', FALSE),
    (6, 'Ne', TRUE),

  (7, '(2,6)', FALSE),
    (7, '(3,6)', FALSE),
    (7, '(2,5)', FALSE),
    (7, '(3,5)', TRUE),

  (8, '08,16,45,12,34', TRUE),
    (8, '08,34,45,01,59', FALSE),
    (8, '04,08,01,89,92', FALSE),
    (8, '43,37,32,29,21', FALSE),

  (9, '21', FALSE),
    (9, '27', FALSE),
    (9, '30', TRUE),
    (9, '24', FALSE),

  (10, '6', FALSE),
    (10, '8', FALSE),
    (10, '7', FALSE),
    (10, '5', TRUE),

  (11, 'Ne', FALSE),
    (11, 'Da', TRUE),

  (12, '4', FALSE),
    (12, '5', FALSE),
    (12, '3', FALSE),
    (12, '2', TRUE),

  (13, '5', FALSE),
    (13, '6', FALSE),
    (13, '3', TRUE),
    (13, '4', FALSE),

  (14, '3-regularni, bridno 3-obojivi grafovi', FALSE),
    (14, '3-regularni, bridno 4-obojivi grafovi', TRUE),
    (14, '3-regularni, 3-obojivi grafovi', FALSE),
    (14, '3-regularni, 4-obojivi grafovi', FALSE),

  (15, 'Most', FALSE),
    (15, 'Eulerovska staza', FALSE),
    (15, 'Ciklus', TRUE),
    (15, 'Hamiltonov put', FALSE),

  (16, '3-regularan graf', FALSE),
    (16, 'bridno 3-obojeni graf', TRUE),
    (16, 'neusmjereni graf', FALSE),
    (16, 'graf bez mostova', FALSE),

  (17, 'Vladimir Vranić', FALSE),
    (17, 'Zvonimir Janko', FALSE),
    (17, 'Rudolf Cesarec', FALSE),
    (17, 'Danilo Blanuša', TRUE),

  (18, 'Da', TRUE),
    (18, 'Ne', FALSE),

  (19, 'Ne', FALSE),
    (19, 'Da', TRUE),

  (20, 'Hamiltonov graf', FALSE),
    (20, 'Eulerovski graf', FALSE),
    (20, 'Bipartitan graf', FALSE),
    (20, 'Graf jedinične udaljenosti', TRUE),

  (21, 'K10', FALSE),
    (21, 'L(K5)', TRUE),
    (21, 'K5', FALSE),
    (21, 'K3,3', FALSE),

  (22, 'Kneserov graf K(n,k) ima n povrh k vrhova', FALSE),
    (22, 'Kneserov graf je tranzitivan', FALSE),
    (22, 'Kneserov graf K(n,1) je graf kotač sa n vrhova', TRUE),

  (23, 'Ne', TRUE),
    (23, 'Da', FALSE),

  (24, 'Krajnji vrh i izolirani vrh', FALSE),
    (24, 'Susjedni i incidentni vrh', FALSE),
    (24, 'Izolirani vrh i krajnji vrh', TRUE),
    (24, 'Incidentni i susjedni vrh', FALSE),

  (25, 'Lema o sumiranju', FALSE),
    (25, 'Lema o rukovanju', TRUE),
    (25, 'Lema o putovanju', FALSE),
    (25, 'Lema o parnosti', FALSE),

  (26, '4', FALSE),
    (26, '2', TRUE),
    (26, '1', FALSE),
    (26, '3', FALSE),

  (27, 'Samo Q1 i Q3 su planarne', FALSE),
    (27, 'Samo Q2 i Q3 su planarne', FALSE),
    (27, 'Samo Q1 i Q2 su planarne', FALSE),
    (27, 'Sve tri su planarne', TRUE),

  (28, '4', TRUE),
    (28, '6', FALSE),
    (28, '5', FALSE),
    (28, '3', FALSE),

  (29, '1 i 0', FALSE),
    (29, '1 i 1', FALSE),
    (29, '0 i 1', TRUE),
    (29, '0 i 0', FALSE),

  (30, 'Ne', FALSE),
    (30, 'Da', TRUE),

  (31, 'Karta nije planarni graf', FALSE),
    (31, 'Karta G je 3-strano obojiva <==> G je eulerovski graf', FALSE),
    (31, 'Kubična karta je 4-regularan graf', FALSE),
    (31, 'Karta je 3-povezan graf', TRUE),

  (32, 'π(G)', FALSE),
    (32, 'χ(G)', TRUE),
    (32, 'λ(G)', FALSE),
    (32, 'μ(G)', FALSE),

  (33, 'Francis Guthrie', TRUE),
    (33, 'Julius Petersen', FALSE),
    (33, 'Danilo Blanuša', FALSE),
    (33, 'Edward F. Moore', FALSE),

  (34, 'Ikosaedar', FALSE),
    (34, 'Dodekaedar', FALSE),
    (34, 'Petersenov graf', TRUE),
    (34, 'Heawoodov graf', FALSE),
    
  (35, 'Da, graf je hamiltonov', TRUE),
    (35, 'Graf je hipohamiltonov', FALSE),
    (35, 'Ne, graf nije hamiltonov', FALSE),

  (36, 'χ´(G) = 5', FALSE),
    (36, 'χ´(G) = ∆+1', FALSE),
    (36, '∆ ≤ χ´(G) ≤ ∆+1', TRUE),
    (36, '∆ = χ´(G)', FALSE)
`;

const sql_insert_quizes = `INSERT INTO quizes (quiz_id, user_id, first_name, last_name, bodovi, datumIgre) VALUES ('1', '1', 'admin', 'adminko', '7', '2022-11-23')
, ('2', '2', 'test', 'testić', '5', '2022-12-08'), ('3', '1', 'admin', 'adminko', '3', '2022-12-03'), ('4', '2', 'test', 'testić', '2', '2022-11-03')`

let table_names = [
    "users",
    "questions",
    "answers",
    "quizes",
    "session"
]

let tables = [
    sql_create_users,
    sql_create_questions,
    sql_create_answers,
    sql_create_quizes,
    sql_create_sessions
];

let table_data = [
    sql_insert_users,
    sql_insert_questions,
    sql_insert_answers,
    sql_insert_quizes,
    undefined
]

let indexes = [
  sql_create_session_index1,
  sql_create_session_index2
]

if ((tables.length !== table_data.length) || (tables.length !== table_names.length)) {
    console.log("tables, names and data arrays length mismatch.")
    
}

(async () => {
    console.log("Creating and populating tables");
    for (let i = 0; i < tables.length; i++) {
        console.log("Creating table " + table_names[i] + ".");
        try {
            await pool.query(tables[i], [])
            console.log("Table " + table_names[i] + " created.");
            if (table_data[i] !== undefined) {
                try {
                    await pool.query(table_data[i], [])
                    console.log("Table " + table_names[i] + " populated with data.");
                } catch (err) {
                    console.log("Error populating table " + table_names[i] + " with data.")
                    return console.log(err.message);
                }
            }
        } catch (err) {
            console.log("Error creating table " + table_names[i])
            return console.log(err.message);
        }
    }

    console.log("Creating indexes");
    for (let i = 0; i < indexes.length; i++) {
        try {
            await pool.query(indexes[i], [])
            console.log("Index " + i + " created.")
        } catch (err) {
            console.log("Error creating index " + i + ".")
        }
    }

    await pool.end();
})()
