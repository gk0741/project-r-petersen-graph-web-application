const db = require('../db')

module.exports = class Quiz {
    constructor(quiz_id, user_id, first_name, last_name, bodovi, datumIgre) {
        this.quiz_id = quiz_id
        this.user_id = user_id
        this.first_name = first_name
        this.last_name = last_name
        this.bodovi = bodovi
        this.datumIgre = datumIgre
    }

    static async fetchLeaderboard() {
        let results = await dbGetTopTenQuizes();
        return results
    }

    static async fetchLeaderboardByUserId(user_id) {
        let results = await dbGetQuizesByUserId(user_id);
        return results
    }

    static async getQuizId(){
        let i = await dbGetQuizCount();

        return i.length + 1;
    }

    async addQuiz() {
        try {
            let quizRes = await dbNewQuiz(this);
            console.log("Added quiz");
        } catch(err) {
            console.log("ERROR persisting quiz data: " + JSON.stringify(this))
            throw err
        }
    }
}

dbGetTopTenQuizes = async () => {
    const sql = `SELECT quiz_id, user_id, first_name, last_name, bodovi, datumIgre
    FROM quizes ORDER BY bodovi DESC LIMIT 10`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetQuizesByUserId = async (user_id) => {
    const sql = `SELECT *
    FROM quizes WHERE user_id = ` + user_id + ` ORDER BY bodovi DESC`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbNewQuiz = async (q) => {
    const sql = "INSERT INTO quizes (quiz_id, user_id, first_name, last_name, bodovi, datumIgre) VALUES (" +
    q.quiz_id + ", " + q.user_id + ", '" + q.first_name + "', '" + q.last_name + "', " + q.bodovi + ", current_date + INTERVAL '1 DAY')";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetQuizCount = async () =>{
    const sql = `SELECT *
    FROM quizes`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}