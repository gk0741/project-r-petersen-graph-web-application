const db = require('../db')

module.exports = class Question {
    constructor(question_id, question_text, question_hint, question_fact, question_image_url) {
        this.question_id = question_id
        this.question_text = question_text
        this.question_hint = question_hint
        this.question_fact = question_fact
        this.question_image_url = question_image_url
    }

    static async fetchQuestions(amount) {
        let results = [];
        for(let idn = 0; idn < amount; idn++){
            let question = await dbGetQuestionById(idn);
            results.push(question);
        }

        return results
    }

    static async fetchQuestionByQuestionId(question_id){
        let question = await dbGetQuestionById(question_id);
        return question
    }

}

dbGetQuestionById = async (question_id) => {
    const sql = `SELECT question_id, question_text, question_hint, question_fact, question_image_url
    FROM questions WHERE question_id = ` + question_id;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}
