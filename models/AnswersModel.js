const db = require('../db')

module.exports = class Answer {
    constructor(answer_id, answer_text, is_correct) {
        this.answer_id = answer_id
        this.answer_text = answer_text
        this.is_correct = is_correct
    }

    static async fetchByAnswerId(answer_id) {
        let results = await dbGetAnswersById(answer_id);
        return results
    }


}
dbGetAnswersById = async (answer_id) => {
    const sql = `SELECT answer_id, answer_text, is_correct
    FROM answers WHERE answer_id = ` + answer_id;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

