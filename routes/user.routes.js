const express = require('express');
const router = express.Router();
const authHandler = require('./helpers/auth-handler');
const Quiz = require('../models/QuizModel');

router.get('/', authHandler, function (req, res, next) {
    (async () => {
        let leaderboard = await Quiz.fetchLeaderboardByUserId(req.session.user.user_id);
        res.render('user', {
            title: 'User profile',
            user: req.session.user,
            leaderboard: leaderboard
        });
    })()
});

module.exports = router;