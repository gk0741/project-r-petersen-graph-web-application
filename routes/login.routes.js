const express = require('express');
const router = express.Router();
const User = require('../models/UserModel')


router.get('/', function (req, res, next) {
    res.render('login', {
        title: 'Login',
        user: req.session.user,
        err: undefined
    })

});

router.post('/', async function (req, res, next) {
    let user=undefined
    user = await User.fetchByUsername(req.body.user)
    console.log(user);
    let errormsg = 'Netočna lozinka'

    if(user && user.checkPassword(req.body.password)){
            req.session.user = user
            res.redirect('/')
    }
    else{
        if(user.user_id === undefined){
            errormsg = 'Netočno korisničko ime ili lozinka.'
        }
        res.render('login', {
            title: 'Login',
            user: req.session.user,
            err: errormsg
        })
    }
});


module.exports = router;