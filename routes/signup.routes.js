const express = require('express');
const router = express.Router();
const User = require('../models/UserModel')


router.get('/', function (req, res, next) {
    res.render('signup', {
        title: 'Register a new user',
        user: req.session.user,
        err: undefined
    });
});

router.post('/', function (req, res, next) {

    (async () => {
        if (req.body.password1 !== req.body.password2) {
            res.render('Signup', {
                title: 'Register a new user',
                user: req.session.user,
                err: "Lozinke se ne poklapaju"
            });
            return;
        }

        let user = await User.fetchByUsername(req.body.username);

        if (user.id !== undefined) {
            res.render('Signup', {
                title: 'Register a new user',
                user: req.session.user,
                err: "Korisničko ime već postoji"
            });
            return;
        } 

        user = new User(req.body.username, req.body.firstName, req.body.lastName, req.body.password1);
        await user.persist();

        req.session.user = user
        res.redirect('/')
    })()

});

module.exports = router;