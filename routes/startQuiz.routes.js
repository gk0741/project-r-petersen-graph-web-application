const express = require('express');
const router = express.Router();
const Question = require('../models/QuestionsModel');
const Answer = require('../models/AnswersModel');
const Quiz = require('../models/QuizModel');

router.get('/',function (req, res, next) {
    (async () => {
        let previousQueId = req.session.previous;
        req.session.previous = [];
        if(previousQueId === undefined){
            previousQueId = [];
        }
        if(req.query.isCorrect === "yes"){
            req.session.corr++;
        }
        let correct = req.session.corr;
        if(correct === undefined){
            correct = 0;
        }

        if(previousQueId.length === 10){
            req.session.previous = [];
            let qzid = await Quiz.getQuizId();
            let quiz = new Quiz(qzid, req.session.user.user_id, req.session.user.first_name, req.session.user.last_name, req.session.corr, "2022-12-08");
            await quiz.addQuiz();
            res.redirect("/resultQuiz");
        }
        else{
            let qid = 0;
            let run = false;
            if(previousQueId.length === 0){
                qid = Math.floor(Math.random() * 35) + 1;
            }
            else{
                do{
                    run = false;
                    qid = Math.floor(Math.random() * 35) + 1;
                    for(let i = 0; i < previousQueId.length; i++){
                        if(qid === previousQueId[i]){
                            run = true;
                        }
                    }
                }while(run);
            }
            let questions = await Question.fetchQuestionByQuestionId(qid);
            previousQueId.push(qid);
            req.session.previous = previousQueId;
            req.session.corr = correct;
            let answers = await Answer.fetchByAnswerId(questions[0].question_id);
            res.render('startQuiz', {
                title: 'Start a quiz',
                user: req.session.user,
                question: questions[0].question_text,
                hint: questions[0].question_hint,
                fact: questions[0].question_fact,
                answers:answers,
                correctAns: correct,
                imageUrl: questions[0].question_image_url           
            });
        }
    })();
});

module.exports = router;