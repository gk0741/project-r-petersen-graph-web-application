const express = require('express');
const router = express.Router();

router.get('/', function (req, res, next) {
    let c = req.session.corr;
    req.session.corr = 0;
    res.render('resultQuiz', {
        title: 'Quiz result',
        user: req.session.user,
        correct: c     
    });
});

module.exports = router;